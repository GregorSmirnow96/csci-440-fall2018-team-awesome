# Team Members

* Jerad Hoy
* Anna Watson
* Greg Smirnow
* Sarah Hall

# Idea 1
Intro to Using Sqlite3 with Java. 

# Idea 2
Top 3 Types of Joins in Sqlite3. Inner, left, left outer, cross.

# Idea 3
Converting ER Models to Relational Diagrams.